#!/usr/bin/env python
# coding: utf-8

# In[51]:


import pyspark
import pyspark.sql.functions as f
from pyspark.sql.types import StringType
from pyspark.sql import SparkSession
from pyspark.sql import Window

spark = SparkSession.builder.appName('cd_carteira_analitic_22022022.py').config('spark.master', 'local').getOrCreate()

an_cd_carteira = spark.read.parquet("D:\Kroton_Tutoriais_20212\Arquivos_Kroton\part-00000-befc38ed-c1ac-4992-8e7f-814bbe09cc0c-c000.snappy.parquet", encoding='ISO-8859-1', header=True, sep=";")

an_cd_carteira_c = an_cd_carteira.select('DAT_PROCESSAMENTO','DSC_SITUACAO_CONTRATO','TIP_CURSO','TIP_MATRICULA','VLR_SALDO_DEV_VENC')                                 .withColumn('VLR_SALDO_DEV_VENC',f.round(f.col('VLR_SALDO_DEV_VENC'),2))                                 .withColumn('DATA',an_cd_carteira['DAT_PROCESSAMENTO'].cast(StringType()))                                 .filter(f.col('DATA').isNotNull()|f.col('DSC_SITUACAO_CONTRATO').isNotNull())

Analitic_A = an_cd_carteira_c.groupBy('DATA','DSC_SITUACAO_CONTRATO','TIP_CURSO','TIP_MATRICULA')                     .count().select('DATA',
                                     'DSC_SITUACAO_CONTRATO',
                                     'TIP_CURSO',
                                     'TIP_MATRICULA',\
                     f.col('count').alias('FreqAb'))\
                      .orderBy('DATA','DSC_SITUACAO_CONTRATO','TIP_CURSO','TIP_MATRICULA')

pi = Window.partitionBy('DATA','TIP_CURSO','TIP_MATRICULA')

Analitic_B = Analitic_A.select('DATA','DSC_SITUACAO_CONTRATO','TIP_CURSO','TIP_MATRICULA','FreqAb',
                               f.sum('FreqAb').over(pi).alias('FreqRel')\
                               ).orderBy('DATA','TIP_MATRICULA','DSC_SITUACAO_CONTRATO','TIP_CURSO')

Analitic_B.select('DATA','DSC_SITUACAO_CONTRATO','TIP_CURSO','TIP_MATRICULA','FreqAb','FreqRel',
                 (f.round(f.col('FreqAb')/f.col('FreqRel')*100,1).alias('Percent')).cast("float")\
                 ).distinct().orderBy('DATA','TIP_CURSO','TIP_MATRICULA','DSC_SITUACAO_CONTRATO')

