{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 51,
   "id": "b7de4374",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "DataFrame[DATA: string, DSC_SITUACAO_CONTRATO: string, TIP_CURSO: string, TIP_MATRICULA: string, FreqAb: bigint, FreqRel: bigint, Percent: float]"
      ]
     },
     "execution_count": 51,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "import pyspark\n",
    "import pyspark.sql.functions as f\n",
    "from pyspark.sql.types import StringType\n",
    "from pyspark.sql import SparkSession\n",
    "from pyspark.sql import Window\n",
    "\n",
    "spark = SparkSession.builder.appName('cd_carteira_analitic_teste.ipynb').config('spark.master', 'local').getOrCreate()\n",
    "\n",
    "an_cd_carteira = spark.read.parquet(\"D:\\Kroton_Tutoriais_20212\\Arquivos_Kroton\\part-00000-befc38ed-c1ac-4992-8e7f-814bbe09cc0c-c000.snappy.parquet\", encoding='ISO-8859-1', header=True, sep=\";\")\n",
    "\n",
    "an_cd_carteira_c = an_cd_carteira.select('DAT_PROCESSAMENTO','DSC_SITUACAO_CONTRATO','TIP_CURSO','TIP_MATRICULA','VLR_SALDO_DEV_VENC')\\\n",
    "                                 .withColumn('VLR_SALDO_DEV_VENC',f.round(f.col('VLR_SALDO_DEV_VENC'),2))\\\n",
    "                                 .withColumn('DATA',an_cd_carteira['DAT_PROCESSAMENTO'].cast(StringType()))\\\n",
    "                                 .filter(f.col('DATA').isNotNull()|f.col('DSC_SITUACAO_CONTRATO').isNotNull())\n",
    "\n",
    "Analitic_A = an_cd_carteira_c.groupBy('DATA','DSC_SITUACAO_CONTRATO','TIP_CURSO','TIP_MATRICULA')\\\n",
    "                     .count().select('DATA',\n",
    "                                     'DSC_SITUACAO_CONTRATO',\n",
    "                                     'TIP_CURSO',\n",
    "                                     'TIP_MATRICULA',\\\n",
    "                     f.col('count').alias('FreqAb'))\\\n",
    "                      .orderBy('DATA','DSC_SITUACAO_CONTRATO','TIP_CURSO','TIP_MATRICULA')\n",
    "\n",
    "pi = Window.partitionBy('DATA','TIP_CURSO','TIP_MATRICULA')\n",
    "\n",
    "Analitic_B = Analitic_A.select('DATA','DSC_SITUACAO_CONTRATO','TIP_CURSO','TIP_MATRICULA','FreqAb',\n",
    "                               f.sum('FreqAb').over(pi).alias('FreqRel')\\\n",
    "                               ).orderBy('DATA','TIP_MATRICULA','DSC_SITUACAO_CONTRATO','TIP_CURSO')\n",
    "\n",
    "Analitic_B.select('DATA','DSC_SITUACAO_CONTRATO','TIP_CURSO','TIP_MATRICULA','FreqAb','FreqRel',\n",
    "                 (f.round(f.col('FreqAb')/f.col('FreqRel')*100,1).alias('Percent')).cast(\"float\")\\\n",
    "                 ).distinct().orderBy('DATA','TIP_CURSO','TIP_MATRICULA','DSC_SITUACAO_CONTRATO')"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
